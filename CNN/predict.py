import os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"] = "1"

import numpy as np
import pandas as pd

import keras
import math
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import losses
import keras.backend as K
import tensorflow as tf
import shutil
from os import listdir

import matplotlib.image as mpimg
from scipy import misc

from sklearn.metrics import roc_auc_score

from keras.initializers import glorot_uniform, RandomNormal, lecun_normal, he_normal
from keras.callbacks import LearningRateScheduler, ModelCheckpoint,ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
from keras.layers.normalization import BatchNormalization


from chembl_webresource_client import *
chembl = CompoundResource()

data_path = "data/"
folder_path = "/publicdata/cellpainting/pngs/"
metadata_path = "/publicdata/cellpainting/Metadata/"


#https://github.com/keras-team/keras/issues/3893
def build_masked_loss(loss_function, mask_value):
    """Builds a loss function that masks based on targets

    Args:
        loss_function: The loss function to mask
        mask_value: The value to mask in the targets

    Returns:
        function: a loss function that acts like loss_function with masked inputs
    """

    def masked_loss_function(y_true, y_pred):
        mask = K.cast(K.not_equal(y_true, mask_value), K.floatx())
        return loss_function(y_true * mask, y_pred * mask)

    return masked_loss_function
    
    
def getPngImage(path, number, a, s):
    folders = ["ERSyto", "ERSytoBleed", "Hoechst", "Mito", "Ph_golgi"]
    J=list()
    global all_max
    for f in folders:
        folder = path + str(number) + "-" + f
        filenames = listdir(folder)
        for filename in filenames:
            if (a.lower() + "_" + s.lower()) in filename.lower():
                imarray = misc.imread(folder+'/'+filename)
                cut_val = np.percentile(imarray, q=99.7)
                imarray = np.clip(imarray, 0, cut_val)
                imarray = np.float32(imarray)
                normalized = (imarray-np.min(imarray))/(np.max(imarray)-np.min(imarray))
                J.append(normalized)
    J = np.stack(J,axis=2)
    shape=J.shape
    I = list()
    I.append(J[:int(shape[0]/2),:int(shape[1]/2),:])
    I.append(J[int(shape[0]/2):,:int(shape[1]/2),:])
    I.append(J[:int(shape[0]/2),int(shape[1]/2):,:])
    I.append(J[int(shape[0]/2):,int(shape[1]/2):,:])
    return np.array(I)
    
    
assays = pd.read_csv(data_path + "data/labels_test.csv").columns[1:]

model = load_model(data_path + "model/model_6.h5", custom_objects={'masked_loss_function': build_masked_loss(my_binary_crossentropy, -1)})

def read_mean_well_profiles_file(filepath):
    cols ="Image_Metadata_PlateID,Image_Metadata_CPD_WELL_POSITION,Image_Metadata_ASSAY_WELL_ROLE,Image_Metadata_BROAD_ID,Image_Metadata_CPD_MMOL_CONC,Image_Metadata_CPD_PLATE_MAP_NAME,Image_Metadata_UserStem,Image_Metadata_Wave,Image_Metadata_Current.Owning.Project,Image_Metadata_Restricted.To.Project,Image_Metadata_Original.Sample,Image_Metadata_Samples.Per.Structure,Image_Metadata_Structure.ID,Image_Metadata_Sample.ID,Image_Metadata_Collection.ID,Image_Metadata_Source,Image_Metadata_Vendor.Catalog.ID,Image_Metadata_Compound.Name,Image_Metadata_Pubchem.SID,Image_Metadata_Pubchem.CID,Image_Metadata_MLSMR.Sample.ID,Image_Metadata_Chemist.or.Library,Image_Metadata_Expected.Mass..Desalted.,Image_Metadata_Registration.Date,Image_Metadata_Smiles,Image_Metadata_Comment,Cells_AreaShape_Area,Cells_AreaShape_Center_X,Cells_AreaShape_Center_Y,Cells_AreaShape_Compactness,Cells_AreaShape_Eccentricity,Cells_AreaShape_Extent,Cells_AreaShape_FormFactor,Cells_AreaShape_MajorAxisLength,Cells_AreaShape_MinorAxisLength,Cells_AreaShape_Orientation,Cells_AreaShape_Perimeter,Cells_AreaShape_Solidity,Cells_AreaShape_Zernike_0_0,Cells_AreaShape_Zernike_1_1,Cells_AreaShape_Zernike_2_0,Cells_AreaShape_Zernike_2_2,Cells_AreaShape_Zernike_3_1,Cells_AreaShape_Zernike_3_3,Cells_AreaShape_Zernike_4_0,Cells_AreaShape_Zernike_4_2,Cells_AreaShape_Zernike_4_4,Cells_AreaShape_Zernike_5_1,Cells_AreaShape_Zernike_5_3,Cells_AreaShape_Zernike_5_5,Cells_AreaShape_Zernike_6_0,Cells_AreaShape_Zernike_6_2,Cells_AreaShape_Zernike_6_4,Cells_AreaShape_Zernike_6_6,Cells_AreaShape_Zernike_7_1,Cells_AreaShape_Zernike_7_3,Cells_AreaShape_Zernike_7_5,Cells_AreaShape_Zernike_7_7,Cells_AreaShape_Zernike_8_0,Cells_AreaShape_Zernike_8_2,Cells_AreaShape_Zernike_8_4,Cells_AreaShape_Zernike_8_6,Cells_AreaShape_Zernike_8_8,Cells_AreaShape_Zernike_9_1,Cells_AreaShape_Zernike_9_3,Cells_AreaShape_Zernike_9_5,Cells_AreaShape_Zernike_9_7,Cells_AreaShape_Zernike_9_9,Cells_Intensity_IntegratedIntensityEdge_ER,Cells_Intensity_IntegratedIntensityEdge_DNA,Cells_Intensity_IntegratedIntensityEdge_Mito,Cells_Intensity_IntegratedIntensityEdge_AGP,Cells_Intensity_IntegratedIntensityEdge_RNA,Cells_Intensity_IntegratedIntensity_ER,Cells_Intensity_IntegratedIntensity_DNA,Cells_Intensity_IntegratedIntensity_Mito,Cells_Intensity_IntegratedIntensity_AGP,Cells_Intensity_IntegratedIntensity_RNA,Cells_Intensity_LowerQuartileIntensity_ER,Cells_Intensity_LowerQuartileIntensity_DNA,Cells_Intensity_LowerQuartileIntensity_Mito,Cells_Intensity_LowerQuartileIntensity_AGP,Cells_Intensity_LowerQuartileIntensity_RNA,Cells_Intensity_MassDisplacement_ER,Cells_Intensity_MassDisplacement_DNA,Cells_Intensity_MassDisplacement_Mito,Cells_Intensity_MassDisplacement_AGP,Cells_Intensity_MassDisplacement_RNA,Cells_Intensity_MaxIntensityEdge_ER,Cells_Intensity_MaxIntensityEdge_DNA,Cells_Intensity_MaxIntensityEdge_Mito,Cells_Intensity_MaxIntensityEdge_AGP,Cells_Intensity_MaxIntensityEdge_RNA,Cells_Intensity_MaxIntensity_ER,Cells_Intensity_MaxIntensity_DNA,Cells_Intensity_MaxIntensity_Mito,Cells_Intensity_MaxIntensity_AGP,Cells_Intensity_MaxIntensity_RNA,Cells_Intensity_MeanIntensityEdge_ER,Cells_Intensity_MeanIntensityEdge_DNA,Cells_Intensity_MeanIntensityEdge_Mito,Cells_Intensity_MeanIntensityEdge_AGP,Cells_Intensity_MeanIntensityEdge_RNA,Cells_Intensity_MeanIntensity_ER,Cells_Intensity_MeanIntensity_DNA,Cells_Intensity_MeanIntensity_Mito,Cells_Intensity_MeanIntensity_AGP,Cells_Intensity_MeanIntensity_RNA,Cells_Intensity_MedianIntensity_ER,Cells_Intensity_MedianIntensity_DNA,Cells_Intensity_MedianIntensity_Mito,Cells_Intensity_MedianIntensity_AGP,Cells_Intensity_MedianIntensity_RNA,Cells_Intensity_MinIntensityEdge_ER,Cells_Intensity_MinIntensityEdge_DNA,Cells_Intensity_MinIntensityEdge_Mito,Cells_Intensity_MinIntensityEdge_AGP,Cells_Intensity_MinIntensityEdge_RNA,Cells_Intensity_MinIntensity_ER,Cells_Intensity_MinIntensity_DNA,Cells_Intensity_MinIntensity_Mito,Cells_Intensity_MinIntensity_AGP,Cells_Intensity_MinIntensity_RNA,Cells_Intensity_StdIntensityEdge_ER,Cells_Intensity_StdIntensityEdge_DNA,Cells_Intensity_StdIntensityEdge_Mito,Cells_Intensity_StdIntensityEdge_AGP,Cells_Intensity_StdIntensityEdge_RNA,Cells_Intensity_StdIntensity_ER,Cells_Intensity_StdIntensity_DNA,Cells_Intensity_StdIntensity_Mito,Cells_Intensity_StdIntensity_AGP,Cells_Intensity_StdIntensity_RNA,Cells_Intensity_UpperQuartileIntensity_ER,Cells_Intensity_UpperQuartileIntensity_DNA,Cells_Intensity_UpperQuartileIntensity_Mito,Cells_Intensity_UpperQuartileIntensity_AGP,Cells_Intensity_UpperQuartileIntensity_RNA,Cells_Location_Center_X,Cells_Location_Center_Y,Cells_Neighbors_AngleBetweenNeighbors_Adjacent,Cells_Neighbors_FirstClosestDistance_Adjacent,Cells_Neighbors_FirstClosestObjectNumber_Adjacent,Cells_Neighbors_NumberOfNeighbors_Adjacent,Cells_Neighbors_PercentTouching_Adjacent,Cells_Neighbors_SecondClosestDistance_Adjacent,Cells_Neighbors_SecondClosestObjectNumber_Adjacent,Cells_Parent_Nuclei,Cells_RadialDistribution_FracAtD_ER_1of4,Cells_RadialDistribution_FracAtD_ER_2of4,Cells_RadialDistribution_FracAtD_ER_3of4,Cells_RadialDistribution_FracAtD_ER_4of4,Cells_RadialDistribution_FracAtD_Mito_1of4,Cells_RadialDistribution_FracAtD_Mito_2of4,Cells_RadialDistribution_FracAtD_Mito_3of4,Cells_RadialDistribution_FracAtD_Mito_4of4,Cells_RadialDistribution_FracAtD_AGP_1of4,Cells_RadialDistribution_FracAtD_AGP_2of4,Cells_RadialDistribution_FracAtD_AGP_3of4,Cells_RadialDistribution_FracAtD_AGP_4of4,Cells_RadialDistribution_FracAtD_RNA_1of4,Cells_RadialDistribution_FracAtD_RNA_2of4,Cells_RadialDistribution_FracAtD_RNA_3of4,Cells_RadialDistribution_FracAtD_RNA_4of4,Cells_RadialDistribution_MeanFrac_ER_1of4,Cells_RadialDistribution_MeanFrac_ER_2of4,Cells_RadialDistribution_MeanFrac_ER_3of4,Cells_RadialDistribution_MeanFrac_ER_4of4,Cells_RadialDistribution_MeanFrac_Mito_1of4,Cells_RadialDistribution_MeanFrac_Mito_2of4,Cells_RadialDistribution_MeanFrac_Mito_3of4,Cells_RadialDistribution_MeanFrac_Mito_4of4,Cells_RadialDistribution_MeanFrac_AGP_1of4,Cells_RadialDistribution_MeanFrac_AGP_2of4,Cells_RadialDistribution_MeanFrac_AGP_3of4,Cells_RadialDistribution_MeanFrac_AGP_4of4,Cells_RadialDistribution_MeanFrac_RNA_1of4,Cells_RadialDistribution_MeanFrac_RNA_2of4,Cells_RadialDistribution_MeanFrac_RNA_3of4,Cells_RadialDistribution_MeanFrac_RNA_4of4,Cells_RadialDistribution_RadialCV_ER_1of4,Cells_RadialDistribution_RadialCV_ER_2of4,Cells_RadialDistribution_RadialCV_ER_3of4,Cells_RadialDistribution_RadialCV_ER_4of4,Cells_RadialDistribution_RadialCV_Mito_1of4,Cells_RadialDistribution_RadialCV_Mito_2of4,Cells_RadialDistribution_RadialCV_Mito_3of4,Cells_RadialDistribution_RadialCV_Mito_4of4,Cells_RadialDistribution_RadialCV_AGP_1of4,Cells_RadialDistribution_RadialCV_AGP_2of4,Cells_RadialDistribution_RadialCV_AGP_3of4,Cells_RadialDistribution_RadialCV_AGP_4of4,Cells_RadialDistribution_RadialCV_RNA_1of4,Cells_RadialDistribution_RadialCV_RNA_2of4,Cells_RadialDistribution_RadialCV_RNA_3of4,Cells_RadialDistribution_RadialCV_RNA_4of4,Cells_Texture_AngularSecondMoment_ER_3,Cells_Texture_AngularSecondMoment_ER_5,Cells_Texture_AngularSecondMoment_DNA_3,Cells_Texture_AngularSecondMoment_DNA_5,Cells_Texture_AngularSecondMoment_Mito_3,Cells_Texture_AngularSecondMoment_Mito_5,Cells_Texture_AngularSecondMoment_AGP_3,Cells_Texture_AngularSecondMoment_AGP_5,Cells_Texture_AngularSecondMoment_RNA_3,Cells_Texture_AngularSecondMoment_RNA_5,Cells_Texture_Contrast_ER_3,Cells_Texture_Contrast_ER_5,Cells_Texture_Contrast_DNA_3,Cells_Texture_Contrast_DNA_5,Cells_Texture_Contrast_Mito_3,Cells_Texture_Contrast_Mito_5,Cells_Texture_Contrast_AGP_3,Cells_Texture_Contrast_AGP_5,Cells_Texture_Contrast_RNA_3,Cells_Texture_Contrast_RNA_5,Cells_Texture_Correlation_ER_3,Cells_Texture_Correlation_ER_5,Cells_Texture_Correlation_DNA_3,Cells_Texture_Correlation_DNA_5,Cells_Texture_Correlation_Mito_3,Cells_Texture_Correlation_Mito_5,Cells_Texture_Correlation_AGP_3,Cells_Texture_Correlation_AGP_5,Cells_Texture_Correlation_RNA_3,Cells_Texture_Correlation_RNA_5,Cells_Texture_DifferenceEntropy_ER_3,Cells_Texture_DifferenceEntropy_ER_5,Cells_Texture_DifferenceEntropy_DNA_3,Cells_Texture_DifferenceEntropy_DNA_5,Cells_Texture_DifferenceEntropy_Mito_3,Cells_Texture_DifferenceEntropy_Mito_5,Cells_Texture_DifferenceEntropy_AGP_3,Cells_Texture_DifferenceEntropy_AGP_5,Cells_Texture_DifferenceEntropy_RNA_3,Cells_Texture_DifferenceEntropy_RNA_5,Cells_Texture_DifferenceVariance_ER_3,Cells_Texture_DifferenceVariance_ER_5,Cells_Texture_DifferenceVariance_DNA_3,Cells_Texture_DifferenceVariance_DNA_5,Cells_Texture_DifferenceVariance_Mito_3,Cells_Texture_DifferenceVariance_Mito_5,Cells_Texture_DifferenceVariance_AGP_3,Cells_Texture_DifferenceVariance_AGP_5,Cells_Texture_DifferenceVariance_RNA_3,Cells_Texture_DifferenceVariance_RNA_5,Cells_Texture_Entropy_ER_3,Cells_Texture_Entropy_ER_5,Cells_Texture_Entropy_DNA_3,Cells_Texture_Entropy_DNA_5,Cells_Texture_Entropy_Mito_3,Cells_Texture_Entropy_Mito_5,Cells_Texture_Entropy_AGP_3,Cells_Texture_Entropy_AGP_5,Cells_Texture_Entropy_RNA_3,Cells_Texture_Entropy_RNA_5,Cells_Texture_Gabor_ER_3,Cells_Texture_Gabor_ER_5,Cells_Texture_Gabor_DNA_3,Cells_Texture_Gabor_DNA_5,Cells_Texture_Gabor_Mito_3,Cells_Texture_Gabor_Mito_5,Cells_Texture_Gabor_AGP_3,Cells_Texture_Gabor_AGP_5,Cells_Texture_Gabor_RNA_3,Cells_Texture_Gabor_RNA_5,Cells_Texture_InfoMeas1_ER_3,Cells_Texture_InfoMeas1_ER_5,Cells_Texture_InfoMeas1_DNA_3,Cells_Texture_InfoMeas1_DNA_5,Cells_Texture_InfoMeas1_Mito_3,Cells_Texture_InfoMeas1_Mito_5,Cells_Texture_InfoMeas1_AGP_3,Cells_Texture_InfoMeas1_AGP_5,Cells_Texture_InfoMeas1_RNA_3,Cells_Texture_InfoMeas1_RNA_5,Cells_Texture_InfoMeas2_ER_3,Cells_Texture_InfoMeas2_ER_5,Cells_Texture_InfoMeas2_DNA_3,Cells_Texture_InfoMeas2_DNA_5,Cells_Texture_InfoMeas2_Mito_3,Cells_Texture_InfoMeas2_Mito_5,Cells_Texture_InfoMeas2_AGP_3,Cells_Texture_InfoMeas2_AGP_5,Cells_Texture_InfoMeas2_RNA_3,Cells_Texture_InfoMeas2_RNA_5,Cells_Texture_InverseDifferenceMoment_ER_3,Cells_Texture_InverseDifferenceMoment_ER_5,Cells_Texture_InverseDifferenceMoment_DNA_3,Cells_Texture_InverseDifferenceMoment_DNA_5,Cells_Texture_InverseDifferenceMoment_Mito_3,Cells_Texture_InverseDifferenceMoment_Mito_5,Cells_Texture_InverseDifferenceMoment_AGP_3,Cells_Texture_InverseDifferenceMoment_AGP_5,Cells_Texture_InverseDifferenceMoment_RNA_3,Cells_Texture_InverseDifferenceMoment_RNA_5,Cells_Texture_SumAverage_ER_3,Cells_Texture_SumAverage_ER_5,Cells_Texture_SumAverage_DNA_3,Cells_Texture_SumAverage_DNA_5,Cells_Texture_SumAverage_Mito_3,Cells_Texture_SumAverage_Mito_5,Cells_Texture_SumAverage_AGP_3,Cells_Texture_SumAverage_AGP_5,Cells_Texture_SumAverage_RNA_3,Cells_Texture_SumAverage_RNA_5,Cells_Texture_SumEntropy_ER_3,Cells_Texture_SumEntropy_ER_5,Cells_Texture_SumEntropy_DNA_3,Cells_Texture_SumEntropy_DNA_5,Cells_Texture_SumEntropy_Mito_3,Cells_Texture_SumEntropy_Mito_5,Cells_Texture_SumEntropy_AGP_3,Cells_Texture_SumEntropy_AGP_5,Cells_Texture_SumEntropy_RNA_3,Cells_Texture_SumEntropy_RNA_5,Cells_Texture_SumVariance_ER_3,Cells_Texture_SumVariance_ER_5,Cells_Texture_SumVariance_DNA_3,Cells_Texture_SumVariance_DNA_5,Cells_Texture_SumVariance_Mito_3,Cells_Texture_SumVariance_Mito_5,Cells_Texture_SumVariance_AGP_3,Cells_Texture_SumVariance_AGP_5,Cells_Texture_SumVariance_RNA_3,Cells_Texture_SumVariance_RNA_5,Cells_Texture_Variance_ER_3,Cells_Texture_Variance_ER_5,Cells_Texture_Variance_DNA_3,Cells_Texture_Variance_DNA_5,Cells_Texture_Variance_Mito_3,Cells_Texture_Variance_Mito_5,Cells_Texture_Variance_AGP_3,Cells_Texture_Variance_AGP_5,Cells_Texture_Variance_RNA_3,Cells_Texture_Variance_RNA_5,Cytoplasm_AreaShape_Area,Cytoplasm_AreaShape_Center_X,Cytoplasm_AreaShape_Center_Y,Cytoplasm_AreaShape_Compactness,Cytoplasm_AreaShape_Eccentricity,Cytoplasm_AreaShape_Extent,Cytoplasm_AreaShape_FormFactor,Cytoplasm_AreaShape_MajorAxisLength,Cytoplasm_AreaShape_MinorAxisLength,Cytoplasm_AreaShape_Orientation,Cytoplasm_AreaShape_Perimeter,Cytoplasm_AreaShape_Solidity,Cytoplasm_AreaShape_Zernike_0_0,Cytoplasm_AreaShape_Zernike_1_1,Cytoplasm_AreaShape_Zernike_2_0,Cytoplasm_AreaShape_Zernike_2_2,Cytoplasm_AreaShape_Zernike_3_1,Cytoplasm_AreaShape_Zernike_3_3,Cytoplasm_AreaShape_Zernike_4_0,Cytoplasm_AreaShape_Zernike_4_2,Cytoplasm_AreaShape_Zernike_4_4,Cytoplasm_AreaShape_Zernike_5_1,Cytoplasm_AreaShape_Zernike_5_3,Cytoplasm_AreaShape_Zernike_5_5,Cytoplasm_AreaShape_Zernike_6_0,Cytoplasm_AreaShape_Zernike_6_2,Cytoplasm_AreaShape_Zernike_6_4,Cytoplasm_AreaShape_Zernike_6_6,Cytoplasm_AreaShape_Zernike_7_1,Cytoplasm_AreaShape_Zernike_7_3,Cytoplasm_AreaShape_Zernike_7_5,Cytoplasm_AreaShape_Zernike_7_7,Cytoplasm_AreaShape_Zernike_8_0,Cytoplasm_AreaShape_Zernike_8_2,Cytoplasm_AreaShape_Zernike_8_4,Cytoplasm_AreaShape_Zernike_8_6,Cytoplasm_AreaShape_Zernike_8_8,Cytoplasm_AreaShape_Zernike_9_1,Cytoplasm_AreaShape_Zernike_9_3,Cytoplasm_AreaShape_Zernike_9_5,Cytoplasm_AreaShape_Zernike_9_7,Cytoplasm_AreaShape_Zernike_9_9,Cytoplasm_Intensity_IntegratedIntensityEdge_ER,Cytoplasm_Intensity_IntegratedIntensityEdge_DNA,Cytoplasm_Intensity_IntegratedIntensityEdge_Mito,Cytoplasm_Intensity_IntegratedIntensityEdge_AGP,Cytoplasm_Intensity_IntegratedIntensityEdge_RNA,Cytoplasm_Intensity_IntegratedIntensity_ER,Cytoplasm_Intensity_IntegratedIntensity_DNA,Cytoplasm_Intensity_IntegratedIntensity_Mito,Cytoplasm_Intensity_IntegratedIntensity_AGP,Cytoplasm_Intensity_IntegratedIntensity_RNA,Cytoplasm_Intensity_LowerQuartileIntensity_ER,Cytoplasm_Intensity_LowerQuartileIntensity_DNA,Cytoplasm_Intensity_LowerQuartileIntensity_Mito,Cytoplasm_Intensity_LowerQuartileIntensity_AGP,Cytoplasm_Intensity_LowerQuartileIntensity_RNA,Cytoplasm_Intensity_MassDisplacement_ER,Cytoplasm_Intensity_MassDisplacement_DNA,Cytoplasm_Intensity_MassDisplacement_Mito,Cytoplasm_Intensity_MassDisplacement_AGP,Cytoplasm_Intensity_MassDisplacement_RNA,Cytoplasm_Intensity_MaxIntensityEdge_ER,Cytoplasm_Intensity_MaxIntensityEdge_DNA,Cytoplasm_Intensity_MaxIntensityEdge_Mito,Cytoplasm_Intensity_MaxIntensityEdge_AGP,Cytoplasm_Intensity_MaxIntensityEdge_RNA,Cytoplasm_Intensity_MaxIntensity_ER,Cytoplasm_Intensity_MaxIntensity_DNA,Cytoplasm_Intensity_MaxIntensity_Mito,Cytoplasm_Intensity_MaxIntensity_AGP,Cytoplasm_Intensity_MaxIntensity_RNA,Cytoplasm_Intensity_MeanIntensityEdge_ER,Cytoplasm_Intensity_MeanIntensityEdge_DNA,Cytoplasm_Intensity_MeanIntensityEdge_Mito,Cytoplasm_Intensity_MeanIntensityEdge_AGP,Cytoplasm_Intensity_MeanIntensityEdge_RNA,Cytoplasm_Intensity_MeanIntensity_ER,Cytoplasm_Intensity_MeanIntensity_DNA,Cytoplasm_Intensity_MeanIntensity_Mito,Cytoplasm_Intensity_MeanIntensity_AGP,Cytoplasm_Intensity_MeanIntensity_RNA,Cytoplasm_Intensity_MedianIntensity_ER,Cytoplasm_Intensity_MedianIntensity_DNA,Cytoplasm_Intensity_MedianIntensity_Mito,Cytoplasm_Intensity_MedianIntensity_AGP,Cytoplasm_Intensity_MedianIntensity_RNA,Cytoplasm_Intensity_MinIntensityEdge_ER,Cytoplasm_Intensity_MinIntensityEdge_DNA,Cytoplasm_Intensity_MinIntensityEdge_Mito,Cytoplasm_Intensity_MinIntensityEdge_AGP,Cytoplasm_Intensity_MinIntensityEdge_RNA,Cytoplasm_Intensity_MinIntensity_ER,Cytoplasm_Intensity_MinIntensity_DNA,Cytoplasm_Intensity_MinIntensity_Mito,Cytoplasm_Intensity_MinIntensity_AGP,Cytoplasm_Intensity_MinIntensity_RNA,Cytoplasm_Intensity_StdIntensityEdge_ER,Cytoplasm_Intensity_StdIntensityEdge_DNA,Cytoplasm_Intensity_StdIntensityEdge_Mito,Cytoplasm_Intensity_StdIntensityEdge_AGP,Cytoplasm_Intensity_StdIntensityEdge_RNA,Cytoplasm_Intensity_StdIntensity_ER,Cytoplasm_Intensity_StdIntensity_DNA,Cytoplasm_Intensity_StdIntensity_Mito,Cytoplasm_Intensity_StdIntensity_AGP,Cytoplasm_Intensity_StdIntensity_RNA,Cytoplasm_Intensity_UpperQuartileIntensity_ER,Cytoplasm_Intensity_UpperQuartileIntensity_DNA,Cytoplasm_Intensity_UpperQuartileIntensity_Mito,Cytoplasm_Intensity_UpperQuartileIntensity_AGP,Cytoplasm_Intensity_UpperQuartileIntensity_RNA,Cytoplasm_Location_Center_X,Cytoplasm_Location_Center_Y,Cytoplasm_Parent_Cells,Cytoplasm_Parent_Nuclei,Cytoplasm_Texture_AngularSecondMoment_ER_3,Cytoplasm_Texture_AngularSecondMoment_ER_5,Cytoplasm_Texture_AngularSecondMoment_DNA_3,Cytoplasm_Texture_AngularSecondMoment_DNA_5,Cytoplasm_Texture_AngularSecondMoment_Mito_3,Cytoplasm_Texture_AngularSecondMoment_Mito_5,Cytoplasm_Texture_AngularSecondMoment_AGP_3,Cytoplasm_Texture_AngularSecondMoment_AGP_5,Cytoplasm_Texture_AngularSecondMoment_RNA_3,Cytoplasm_Texture_AngularSecondMoment_RNA_5,Cytoplasm_Texture_Contrast_ER_3,Cytoplasm_Texture_Contrast_ER_5,Cytoplasm_Texture_Contrast_DNA_3,Cytoplasm_Texture_Contrast_DNA_5,Cytoplasm_Texture_Contrast_Mito_3,Cytoplasm_Texture_Contrast_Mito_5,Cytoplasm_Texture_Contrast_AGP_3,Cytoplasm_Texture_Contrast_AGP_5,Cytoplasm_Texture_Contrast_RNA_3,Cytoplasm_Texture_Contrast_RNA_5,Cytoplasm_Texture_Correlation_ER_3,Cytoplasm_Texture_Correlation_ER_5,Cytoplasm_Texture_Correlation_DNA_3,Cytoplasm_Texture_Correlation_DNA_5,Cytoplasm_Texture_Correlation_Mito_3,Cytoplasm_Texture_Correlation_Mito_5,Cytoplasm_Texture_Correlation_AGP_3,Cytoplasm_Texture_Correlation_AGP_5,Cytoplasm_Texture_Correlation_RNA_3,Cytoplasm_Texture_Correlation_RNA_5,Cytoplasm_Texture_DifferenceEntropy_ER_3,Cytoplasm_Texture_DifferenceEntropy_ER_5,Cytoplasm_Texture_DifferenceEntropy_DNA_3,Cytoplasm_Texture_DifferenceEntropy_DNA_5,Cytoplasm_Texture_DifferenceEntropy_Mito_3,Cytoplasm_Texture_DifferenceEntropy_Mito_5,Cytoplasm_Texture_DifferenceEntropy_AGP_3,Cytoplasm_Texture_DifferenceEntropy_AGP_5,Cytoplasm_Texture_DifferenceEntropy_RNA_3,Cytoplasm_Texture_DifferenceEntropy_RNA_5,Cytoplasm_Texture_DifferenceVariance_ER_3,Cytoplasm_Texture_DifferenceVariance_ER_5,Cytoplasm_Texture_DifferenceVariance_DNA_3,Cytoplasm_Texture_DifferenceVariance_DNA_5,Cytoplasm_Texture_DifferenceVariance_Mito_3,Cytoplasm_Texture_DifferenceVariance_Mito_5,Cytoplasm_Texture_DifferenceVariance_AGP_3,Cytoplasm_Texture_DifferenceVariance_AGP_5,Cytoplasm_Texture_DifferenceVariance_RNA_3,Cytoplasm_Texture_DifferenceVariance_RNA_5,Cytoplasm_Texture_Entropy_ER_3,Cytoplasm_Texture_Entropy_ER_5,Cytoplasm_Texture_Entropy_DNA_3,Cytoplasm_Texture_Entropy_DNA_5,Cytoplasm_Texture_Entropy_Mito_3,Cytoplasm_Texture_Entropy_Mito_5,Cytoplasm_Texture_Entropy_AGP_3,Cytoplasm_Texture_Entropy_AGP_5,Cytoplasm_Texture_Entropy_RNA_3,Cytoplasm_Texture_Entropy_RNA_5,Cytoplasm_Texture_Gabor_ER_3,Cytoplasm_Texture_Gabor_ER_5,Cytoplasm_Texture_Gabor_DNA_3,Cytoplasm_Texture_Gabor_DNA_5,Cytoplasm_Texture_Gabor_Mito_3,Cytoplasm_Texture_Gabor_Mito_5,Cytoplasm_Texture_Gabor_AGP_3,Cytoplasm_Texture_Gabor_AGP_5,Cytoplasm_Texture_Gabor_RNA_3,Cytoplasm_Texture_Gabor_RNA_5,Cytoplasm_Texture_InfoMeas1_ER_3,Cytoplasm_Texture_InfoMeas1_ER_5,Cytoplasm_Texture_InfoMeas1_DNA_3,Cytoplasm_Texture_InfoMeas1_DNA_5,Cytoplasm_Texture_InfoMeas1_Mito_3,Cytoplasm_Texture_InfoMeas1_Mito_5,Cytoplasm_Texture_InfoMeas1_AGP_3,Cytoplasm_Texture_InfoMeas1_AGP_5,Cytoplasm_Texture_InfoMeas1_RNA_3,Cytoplasm_Texture_InfoMeas1_RNA_5,Cytoplasm_Texture_InfoMeas2_ER_3,Cytoplasm_Texture_InfoMeas2_ER_5,Cytoplasm_Texture_InfoMeas2_DNA_3,Cytoplasm_Texture_InfoMeas2_DNA_5,Cytoplasm_Texture_InfoMeas2_Mito_3,Cytoplasm_Texture_InfoMeas2_Mito_5,Cytoplasm_Texture_InfoMeas2_AGP_3,Cytoplasm_Texture_InfoMeas2_AGP_5,Cytoplasm_Texture_InfoMeas2_RNA_3,Cytoplasm_Texture_InfoMeas2_RNA_5,Cytoplasm_Texture_InverseDifferenceMoment_ER_3,Cytoplasm_Texture_InverseDifferenceMoment_ER_5,Cytoplasm_Texture_InverseDifferenceMoment_DNA_3,Cytoplasm_Texture_InverseDifferenceMoment_DNA_5,Cytoplasm_Texture_InverseDifferenceMoment_Mito_3,Cytoplasm_Texture_InverseDifferenceMoment_Mito_5,Cytoplasm_Texture_InverseDifferenceMoment_AGP_3,Cytoplasm_Texture_InverseDifferenceMoment_AGP_5,Cytoplasm_Texture_InverseDifferenceMoment_RNA_3,Cytoplasm_Texture_InverseDifferenceMoment_RNA_5,Cytoplasm_Texture_SumAverage_ER_3,Cytoplasm_Texture_SumAverage_ER_5,Cytoplasm_Texture_SumAverage_DNA_3,Cytoplasm_Texture_SumAverage_DNA_5,Cytoplasm_Texture_SumAverage_Mito_3,Cytoplasm_Texture_SumAverage_Mito_5,Cytoplasm_Texture_SumAverage_AGP_3,Cytoplasm_Texture_SumAverage_AGP_5,Cytoplasm_Texture_SumAverage_RNA_3,Cytoplasm_Texture_SumAverage_RNA_5,Cytoplasm_Texture_SumEntropy_ER_3,Cytoplasm_Texture_SumEntropy_ER_5,Cytoplasm_Texture_SumEntropy_DNA_3,Cytoplasm_Texture_SumEntropy_DNA_5,Cytoplasm_Texture_SumEntropy_Mito_3,Cytoplasm_Texture_SumEntropy_Mito_5,Cytoplasm_Texture_SumEntropy_AGP_3,Cytoplasm_Texture_SumEntropy_AGP_5,Cytoplasm_Texture_SumEntropy_RNA_3,Cytoplasm_Texture_SumEntropy_RNA_5,Cytoplasm_Texture_SumVariance_ER_3,Cytoplasm_Texture_SumVariance_ER_5,Cytoplasm_Texture_SumVariance_DNA_3,Cytoplasm_Texture_SumVariance_DNA_5,Cytoplasm_Texture_SumVariance_Mito_3,Cytoplasm_Texture_SumVariance_Mito_5,Cytoplasm_Texture_SumVariance_AGP_3,Cytoplasm_Texture_SumVariance_AGP_5,Cytoplasm_Texture_SumVariance_RNA_3,Cytoplasm_Texture_SumVariance_RNA_5,Cytoplasm_Texture_Variance_ER_3,Cytoplasm_Texture_Variance_ER_5,Cytoplasm_Texture_Variance_DNA_3,Cytoplasm_Texture_Variance_DNA_5,Cytoplasm_Texture_Variance_Mito_3,Cytoplasm_Texture_Variance_Mito_5,Cytoplasm_Texture_Variance_AGP_3,Cytoplasm_Texture_Variance_AGP_5,Cytoplasm_Texture_Variance_RNA_3,Cytoplasm_Texture_Variance_RNA_5,Nuclei_AreaShape_Area,Nuclei_AreaShape_Center_X,Nuclei_AreaShape_Center_Y,Nuclei_AreaShape_Compactness,Nuclei_AreaShape_Eccentricity,Nuclei_AreaShape_Extent,Nuclei_AreaShape_FormFactor,Nuclei_AreaShape_MajorAxisLength,Nuclei_AreaShape_MinorAxisLength,Nuclei_AreaShape_Orientation,Nuclei_AreaShape_Perimeter,Nuclei_AreaShape_Solidity,Nuclei_AreaShape_Zernike_0_0,Nuclei_AreaShape_Zernike_1_1,Nuclei_AreaShape_Zernike_2_0,Nuclei_AreaShape_Zernike_2_2,Nuclei_AreaShape_Zernike_3_1,Nuclei_AreaShape_Zernike_3_3,Nuclei_AreaShape_Zernike_4_0,Nuclei_AreaShape_Zernike_4_2,Nuclei_AreaShape_Zernike_4_4,Nuclei_AreaShape_Zernike_5_1,Nuclei_AreaShape_Zernike_5_3,Nuclei_AreaShape_Zernike_5_5,Nuclei_AreaShape_Zernike_6_0,Nuclei_AreaShape_Zernike_6_2,Nuclei_AreaShape_Zernike_6_4,Nuclei_AreaShape_Zernike_6_6,Nuclei_AreaShape_Zernike_7_1,Nuclei_AreaShape_Zernike_7_3,Nuclei_AreaShape_Zernike_7_5,Nuclei_AreaShape_Zernike_7_7,Nuclei_AreaShape_Zernike_8_0,Nuclei_AreaShape_Zernike_8_2,Nuclei_AreaShape_Zernike_8_4,Nuclei_AreaShape_Zernike_8_6,Nuclei_AreaShape_Zernike_8_8,Nuclei_AreaShape_Zernike_9_1,Nuclei_AreaShape_Zernike_9_3,Nuclei_AreaShape_Zernike_9_5,Nuclei_AreaShape_Zernike_9_7,Nuclei_AreaShape_Zernike_9_9,Nuclei_Intensity_IntegratedIntensityEdge_ER,Nuclei_Intensity_IntegratedIntensityEdge_DNA,Nuclei_Intensity_IntegratedIntensityEdge_Mito,Nuclei_Intensity_IntegratedIntensityEdge_AGP,Nuclei_Intensity_IntegratedIntensityEdge_RNA,Nuclei_Intensity_IntegratedIntensity_ER,Nuclei_Intensity_IntegratedIntensity_DNA,Nuclei_Intensity_IntegratedIntensity_Mito,Nuclei_Intensity_IntegratedIntensity_AGP,Nuclei_Intensity_IntegratedIntensity_RNA,Nuclei_Intensity_LowerQuartileIntensity_ER,Nuclei_Intensity_LowerQuartileIntensity_DNA,Nuclei_Intensity_LowerQuartileIntensity_Mito,Nuclei_Intensity_LowerQuartileIntensity_AGP,Nuclei_Intensity_LowerQuartileIntensity_RNA,Nuclei_Intensity_MassDisplacement_ER,Nuclei_Intensity_MassDisplacement_DNA,Nuclei_Intensity_MassDisplacement_Mito,Nuclei_Intensity_MassDisplacement_AGP,Nuclei_Intensity_MassDisplacement_RNA,Nuclei_Intensity_MaxIntensityEdge_ER,Nuclei_Intensity_MaxIntensityEdge_DNA,Nuclei_Intensity_MaxIntensityEdge_Mito,Nuclei_Intensity_MaxIntensityEdge_AGP,Nuclei_Intensity_MaxIntensityEdge_RNA,Nuclei_Intensity_MaxIntensity_ER,Nuclei_Intensity_MaxIntensity_DNA,Nuclei_Intensity_MaxIntensity_Mito,Nuclei_Intensity_MaxIntensity_AGP,Nuclei_Intensity_MaxIntensity_RNA,Nuclei_Intensity_MeanIntensityEdge_ER,Nuclei_Intensity_MeanIntensityEdge_DNA,Nuclei_Intensity_MeanIntensityEdge_Mito,Nuclei_Intensity_MeanIntensityEdge_AGP,Nuclei_Intensity_MeanIntensityEdge_RNA,Nuclei_Intensity_MeanIntensity_ER,Nuclei_Intensity_MeanIntensity_DNA,Nuclei_Intensity_MeanIntensity_Mito,Nuclei_Intensity_MeanIntensity_AGP,Nuclei_Intensity_MeanIntensity_RNA,Nuclei_Intensity_MedianIntensity_ER,Nuclei_Intensity_MedianIntensity_DNA,Nuclei_Intensity_MedianIntensity_Mito,Nuclei_Intensity_MedianIntensity_AGP,Nuclei_Intensity_MedianIntensity_RNA,Nuclei_Intensity_MinIntensityEdge_ER,Nuclei_Intensity_MinIntensityEdge_DNA,Nuclei_Intensity_MinIntensityEdge_Mito,Nuclei_Intensity_MinIntensityEdge_AGP,Nuclei_Intensity_MinIntensityEdge_RNA,Nuclei_Intensity_MinIntensity_ER,Nuclei_Intensity_MinIntensity_DNA,Nuclei_Intensity_MinIntensity_Mito,Nuclei_Intensity_MinIntensity_AGP,Nuclei_Intensity_MinIntensity_RNA,Nuclei_Intensity_StdIntensityEdge_ER,Nuclei_Intensity_StdIntensityEdge_DNA,Nuclei_Intensity_StdIntensityEdge_Mito,Nuclei_Intensity_StdIntensityEdge_AGP,Nuclei_Intensity_StdIntensityEdge_RNA,Nuclei_Intensity_StdIntensity_ER,Nuclei_Intensity_StdIntensity_DNA,Nuclei_Intensity_StdIntensity_Mito,Nuclei_Intensity_StdIntensity_AGP,Nuclei_Intensity_StdIntensity_RNA,Nuclei_Intensity_UpperQuartileIntensity_ER,Nuclei_Intensity_UpperQuartileIntensity_DNA,Nuclei_Intensity_UpperQuartileIntensity_Mito,Nuclei_Intensity_UpperQuartileIntensity_AGP,Nuclei_Intensity_UpperQuartileIntensity_RNA,Nuclei_Location_Center_X,Nuclei_Location_Center_Y,Nuclei_Neighbors_AngleBetweenNeighbors_Adjacent,Nuclei_Neighbors_FirstClosestDistance_Adjacent,Nuclei_Neighbors_FirstClosestObjectNumber_Adjacent,Nuclei_Neighbors_SecondClosestDistance_Adjacent,Nuclei_Neighbors_SecondClosestObjectNumber_Adjacent,Nuclei_Texture_AngularSecondMoment_ER_3,Nuclei_Texture_AngularSecondMoment_ER_5,Nuclei_Texture_AngularSecondMoment_DNA_3,Nuclei_Texture_AngularSecondMoment_DNA_5,Nuclei_Texture_AngularSecondMoment_Mito_3,Nuclei_Texture_AngularSecondMoment_Mito_5,Nuclei_Texture_AngularSecondMoment_AGP_3,Nuclei_Texture_AngularSecondMoment_AGP_5,Nuclei_Texture_AngularSecondMoment_RNA_3,Nuclei_Texture_AngularSecondMoment_RNA_5,Nuclei_Texture_Contrast_ER_3,Nuclei_Texture_Contrast_ER_5,Nuclei_Texture_Contrast_DNA_3,Nuclei_Texture_Contrast_DNA_5,Nuclei_Texture_Contrast_Mito_3,Nuclei_Texture_Contrast_Mito_5,Nuclei_Texture_Contrast_AGP_3,Nuclei_Texture_Contrast_AGP_5,Nuclei_Texture_Contrast_RNA_3,Nuclei_Texture_Contrast_RNA_5,Nuclei_Texture_Correlation_ER_3,Nuclei_Texture_Correlation_ER_5,Nuclei_Texture_Correlation_DNA_3,Nuclei_Texture_Correlation_DNA_5,Nuclei_Texture_Correlation_Mito_3,Nuclei_Texture_Correlation_Mito_5,Nuclei_Texture_Correlation_AGP_3,Nuclei_Texture_Correlation_AGP_5,Nuclei_Texture_Correlation_RNA_3,Nuclei_Texture_Correlation_RNA_5,Nuclei_Texture_DifferenceEntropy_ER_3,Nuclei_Texture_DifferenceEntropy_ER_5,Nuclei_Texture_DifferenceEntropy_DNA_3,Nuclei_Texture_DifferenceEntropy_DNA_5,Nuclei_Texture_DifferenceEntropy_Mito_3,Nuclei_Texture_DifferenceEntropy_Mito_5,Nuclei_Texture_DifferenceEntropy_AGP_3,Nuclei_Texture_DifferenceEntropy_AGP_5,Nuclei_Texture_DifferenceEntropy_RNA_3,Nuclei_Texture_DifferenceEntropy_RNA_5,Nuclei_Texture_DifferenceVariance_ER_3,Nuclei_Texture_DifferenceVariance_ER_5,Nuclei_Texture_DifferenceVariance_DNA_3,Nuclei_Texture_DifferenceVariance_DNA_5,Nuclei_Texture_DifferenceVariance_Mito_3,Nuclei_Texture_DifferenceVariance_Mito_5,Nuclei_Texture_DifferenceVariance_AGP_3,Nuclei_Texture_DifferenceVariance_AGP_5,Nuclei_Texture_DifferenceVariance_RNA_3,Nuclei_Texture_DifferenceVariance_RNA_5,Nuclei_Texture_Entropy_ER_3,Nuclei_Texture_Entropy_ER_5,Nuclei_Texture_Entropy_DNA_3,Nuclei_Texture_Entropy_DNA_5,Nuclei_Texture_Entropy_Mito_3,Nuclei_Texture_Entropy_Mito_5,Nuclei_Texture_Entropy_AGP_3,Nuclei_Texture_Entropy_AGP_5,Nuclei_Texture_Entropy_RNA_3,Nuclei_Texture_Entropy_RNA_5,Nuclei_Texture_Gabor_ER_3,Nuclei_Texture_Gabor_ER_5,Nuclei_Texture_Gabor_DNA_3,Nuclei_Texture_Gabor_DNA_5,Nuclei_Texture_Gabor_Mito_3,Nuclei_Texture_Gabor_Mito_5,Nuclei_Texture_Gabor_AGP_3,Nuclei_Texture_Gabor_AGP_5,Nuclei_Texture_Gabor_RNA_3,Nuclei_Texture_Gabor_RNA_5,Nuclei_Texture_InfoMeas1_ER_3,Nuclei_Texture_InfoMeas1_ER_5,Nuclei_Texture_InfoMeas1_DNA_3,Nuclei_Texture_InfoMeas1_DNA_5,Nuclei_Texture_InfoMeas1_Mito_3,Nuclei_Texture_InfoMeas1_Mito_5,Nuclei_Texture_InfoMeas1_AGP_3,Nuclei_Texture_InfoMeas1_AGP_5,Nuclei_Texture_InfoMeas1_RNA_3,Nuclei_Texture_InfoMeas1_RNA_5,Nuclei_Texture_InfoMeas2_ER_3,Nuclei_Texture_InfoMeas2_ER_5,Nuclei_Texture_InfoMeas2_DNA_3,Nuclei_Texture_InfoMeas2_DNA_5,Nuclei_Texture_InfoMeas2_Mito_3,Nuclei_Texture_InfoMeas2_Mito_5,Nuclei_Texture_InfoMeas2_AGP_3,Nuclei_Texture_InfoMeas2_AGP_5,Nuclei_Texture_InfoMeas2_RNA_3,Nuclei_Texture_InfoMeas2_RNA_5,Nuclei_Texture_InverseDifferenceMoment_ER_3,Nuclei_Texture_InverseDifferenceMoment_ER_5,Nuclei_Texture_InverseDifferenceMoment_DNA_3,Nuclei_Texture_InverseDifferenceMoment_DNA_5,Nuclei_Texture_InverseDifferenceMoment_Mito_3,Nuclei_Texture_InverseDifferenceMoment_Mito_5,Nuclei_Texture_InverseDifferenceMoment_AGP_3,Nuclei_Texture_InverseDifferenceMoment_AGP_5,Nuclei_Texture_InverseDifferenceMoment_RNA_3,Nuclei_Texture_InverseDifferenceMoment_RNA_5,Nuclei_Texture_SumAverage_ER_3,Nuclei_Texture_SumAverage_ER_5,Nuclei_Texture_SumAverage_DNA_3,Nuclei_Texture_SumAverage_DNA_5,Nuclei_Texture_SumAverage_Mito_3,Nuclei_Texture_SumAverage_Mito_5,Nuclei_Texture_SumAverage_AGP_3,Nuclei_Texture_SumAverage_AGP_5,Nuclei_Texture_SumAverage_RNA_3,Nuclei_Texture_SumAverage_RNA_5,Nuclei_Texture_SumEntropy_ER_3,Nuclei_Texture_SumEntropy_ER_5,Nuclei_Texture_SumEntropy_DNA_3,Nuclei_Texture_SumEntropy_DNA_5,Nuclei_Texture_SumEntropy_Mito_3,Nuclei_Texture_SumEntropy_Mito_5,Nuclei_Texture_SumEntropy_AGP_3,Nuclei_Texture_SumEntropy_AGP_5,Nuclei_Texture_SumEntropy_RNA_3,Nuclei_Texture_SumEntropy_RNA_5,Nuclei_Texture_SumVariance_ER_3,Nuclei_Texture_SumVariance_ER_5,Nuclei_Texture_SumVariance_DNA_3,Nuclei_Texture_SumVariance_DNA_5,Nuclei_Texture_SumVariance_Mito_3,Nuclei_Texture_SumVariance_Mito_5,Nuclei_Texture_SumVariance_AGP_3,Nuclei_Texture_SumVariance_AGP_5,Nuclei_Texture_SumVariance_RNA_3,Nuclei_Texture_SumVariance_RNA_5,Nuclei_Texture_Variance_ER_3,Nuclei_Texture_Variance_ER_5,Nuclei_Texture_Variance_DNA_3,Nuclei_Texture_Variance_DNA_5,Nuclei_Texture_Variance_Mito_3,Nuclei_Texture_Variance_Mito_5,Nuclei_Texture_Variance_AGP_3,Nuclei_Texture_Variance_AGP_5,Nuclei_Texture_Variance_RNA_3,Nuclei_Texture_Variance_RNA_5"
    cols = cols.split(',')
    
    metadata = pd.DataFrame(columns=cols)
    
    f_in = open(filepath, 'r')
    for line in f_in.readlines():
        if "compound" in line:
            split_line = line.split(",")
            if len(split_line) == 851:
                metadata.loc[len(metadata)] = split_line
            else:
                left = split_line[:17]
                right = split_line[-833:]
                middle = split_line[17:-833]
                middle = ",".join(middle)
                row = left
                row.append(middle)
                row.extend(right)
                metadata.loc[len(metadata)] = row
    f_in.close()
    return metadata


subfolders = [f.path for f in os.scandir(metadata_path) if f.is_dir()]
for folder in subfolders:
    if "Plate" in folder:
        path = folder + "/profiles/mean_well_profiles.csv"
        data = read_mean_well_profiles_file(path)
        
        cols = ["plate_id", "well_pos", "broad_id", "smiles", "cpd_name", "chembl_id"]
        for a in assays:
            cols.append(a)
        
        prediction_df = pd.DataFrame(columns=cols)
        
        
        for index, row in data.iterrows():
            if row['Image_Metadata_ASSAY_WELL_ROLE'] == "compound":
                plate_id = row['Image_Metadata_PlateID']
                well_pos = row['Image_Metadata_CPD_WELL_POSITION']
                sites = list(range(6))
                
                all_sites = []
                
                for i in sites:
                    s = "s" + str(i+1)
                    
                    try:
                        img = getPngImage(folder_path, plate_id, well_pos, s)
                    except Exception as e:
                        print(plate_id)
                        print(well_pos)
                        print(s)

                    if np.array(img).shape[3]==10:
                        img = np.array(img)[:,:,:,[0,2,4,6,8]]
        
                    prediction = model.predict(img)
                    mean_pred = np.mean(prediction, axis=0 )
                    mean_pred = np.asarray(mean_pred)

                    all_sites.append(mean_pred)
                    
                    
                all_sites = np.asarray(all_sites)

                predictions = np.mean(all_sites, axis=0)
                
                broad = row['Image_Metadata_BROAD_ID']
                smiles = row['Image_Metadata_Smiles']
                cpd_name = row['Image_Metadata_Compound.Name']
                cpd_name = cpd_name.replace(",",";")

                try:
                    c = chembl.get(smiles=smiles)
                    chembl_id = c[0]['chemblId']
                except Exception as e:
                    chembl_id = ""
               
                new_row = [plate_id, well_pos, broad, smiles, cpd_name, chembl_id]
                for p in predictions:
                    new_row.append("{0:.4f}".format(round(p,4)))
                prediction_df.loc[len(prediction_df)] = new_row
                
            
        print(len(prediction_df))
        prediction_df.to_csv(data_path + "predictions/predictions_" + folder.split("_")[-1].split(":")[0] + ".csv")

        



def getPngBatch(folder_path, folder_number, a, chembl_id, site):
   
    images = getPngImage(folder_path, folder_number, a, site)

    l=[]
    label = getLabelsForCompound(labels,chembl_id)
    for i in range(0,len(images)):
        l.append(label)
    
    if np.array(images).shape[3]==10:
        images = np.array(images)[:,:,:,[0,2,4,6,8]]
    
    images = np.asarray(np.float32(images))
    return images, np.asarray(np.float32(l))

    
    
    


    
    
    
    
    