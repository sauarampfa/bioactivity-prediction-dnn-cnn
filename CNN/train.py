# coding: utf-8

import pandas as pd
import numpy as np
seed = 1
np.random.seed(seed)
import datetime

import sys
if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO, BytesIO

import os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   
os.environ["CUDA_VISIBLE_DEVICES"] = "1"
    
import keras
import math
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers.noise import AlphaDropout
import keras.backend as K
import tensorflow as tf
from os import listdir

from scipy import misc

from sklearn.metrics import roc_auc_score

from keras.initializers import he_normal
from keras.callbacks import LearningRateScheduler, ModelCheckpoint
from keras.layers.normalization import BatchNormalization

all_max = 0

data_path = "data/"
folder_path = "/publicdata/cellpainting/pngs/"
metadata_path = "/publicdata/cellpainting/Metadata/"
log_path = "logs/"
labels_path = "data/"
results_path = "results/"

#https://github.com/keras-team/keras/issues/3893
def build_masked_loss(loss_function, mask_value):
    """Builds a loss function that masks based on targets

    Args:
        loss_function: The loss function to mask
        mask_value: The value to mask in the targets

    Returns:
        function: a loss function that acts like loss_function with masked inputs
    """

    def masked_loss_function(y_true, y_pred):
        mask = K.cast(K.not_equal(y_true, mask_value), K.floatx())
        return loss_function(y_true * mask, y_pred * mask)

    return masked_loss_function
                                 

class saveLoss(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.loss = []
        self.val_loss = []
    def on_epoch_end(self, epoch, logs={}):
        self.loss.append(logs['loss'])
        self.val_loss.append(logs['val_loss'])
        np.save(results_path + 'loss.npy', self.loss)
        np.save(results_path + 'val_loss.npy', self.val_loss)
        
class saveModelOnEpochEnd(keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs={}):
        self.model.save(results_path + "model_" + str(epoch) + ".h5")


    
class ROC_AUC(keras.callbacks.Callback):
    def __init__(self, validation_generator, validation_steps, name):
        super(ROC_AUC, self).__init__()
        self.validation_generator = validation_generator
        self.validation_steps = validation_steps
        self.name = name
        
    def on_train_begin(self, logs={}):
        self.aucs = []
        self.all_mean_aucs=[]
    
    def on_epoch_begin(self, epoch, logs={}):
        self.aucs = []
        
    def on_epoch_end(self, epoch, logs={}):
        try:
            val_step = 0
            all_predictions=[]
            all_true_classes=[]
            for data, true_classes in self.validation_generator:
                if val_step < self.validation_steps:
                    val_step+=1
                    predictions = self.model.predict_on_batch(data)
                    for true_v, pred_v in zip(true_classes, predictions):
                        all_predictions.append(pred_v)
                        all_true_classes.append(true_v)
                else:
                    break
            all_true_classes = np.asarray(all_true_classes)#.T
            all_predictions = np.asarray(all_predictions)#.T

            
            for col in range(all_true_classes.shape[1]):
                idx = np.where(all_true_classes[:,col] > -1)
                true_values = all_true_classes[idx,col][0]
                pred_values = all_predictions[idx,col][0]
                try:
                    auc = roc_auc_score(y_true = true_values, y_score = np.round(pred_values))
                    self.aucs.append(auc)
                    
                except Exception as e:
                    self.aucs.append(0.5)
            time = str(datetime.datetime.now())
            np.save(results_path + "true_" + str(epoch) + '_' + self.name + ".npy", all_true_classes)
            np.save(results_path + "pred_" + str(epoch) + '_' + self.name + ".npy", all_predictions)
            print("AUC: " + str(np.mean(self.aucs)))
            self.all_mean_aucs.append(np.mean(self.aucs))
            logs['val_auc'] = np.mean(self.aucs)
            
            np.save(results_path + self.name + '_aucs.npy', self.all_mean_aucs)
            
            max_5_indices = np.array(self.aucs).argsort()[-5:][::-1]
            print("max auc: " + str(max_5_indices))
            max_5_values = np.array(self.aucs)[max_5_indices]
            print("max auc: " + str(max_5_values))
            
        except Exception as e:
            print("exception in auc")
            print(str(e))
        return


labels = pd.read_csv(labels_path + "labels.csv")
labels.set_index('Unnamed: 0', inplace=True)
labels.index.name = None

folder_numbers = range(24277, 26796)

metadata_train = pd.read_csv(labels_path + "meta_train.csv")
metadata_val = pd.read_csv(labels_path + "meta_val.csv")
metadata_test = pd.read_csv(labels_path + "meta_test.csv")

samples_per_batch = 5

#restrict amount of samples to be able to divide them without overflow by samples_per_batch
train_len = int(len(metadata_train)/samples_per_batch)*samples_per_batch
metadata_train = metadata_train[:train_len]
val_len = int(len(metadata_val)/samples_per_batch)*samples_per_batch
metadata_val = metadata_val[:val_len]
test_len = int(len(metadata_test)/samples_per_batch)*samples_per_batch
metadata_test = metadata_test[:test_len]

metadata_train = metadata_train.sample(frac=1).reset_index(drop=True)
metadata_val = metadata_val.sample(frac=1).reset_index(drop=True)
metadata_test = metadata_test.sample(frac=1).reset_index(drop=True)


he_normal = he_normal(seed=seed)
l1_l2_reg = keras.regularizers.l1_l2(l1=1e-7, l2=1e-5)


def createModel(train_shape, nmb_classes):
    conv_dropout = 0.0
    dense_dropout = 0.5
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same',input_shape=train_shape, kernel_initializer=he_normal, kernel_regularizer=l1_l2_reg))
    model.add(Conv2D(32, (3, 3), padding='same', kernel_initializer=he_normal, kernel_regularizer=l1_l2_reg))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(Dropout(conv_dropout))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    model.add(Conv2D(64, (3, 3), padding='same', kernel_initializer=he_normal, kernel_regularizer=l1_l2_reg))
    model.add(Conv2D(64, (3, 3), padding='same', kernel_initializer=he_normal, kernel_regularizer=l1_l2_reg))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(Dropout(conv_dropout))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    model.add(Conv2D(128, (3, 3), padding='same', kernel_initializer=he_normal, kernel_regularizer=l1_l2_reg))
    model.add(Conv2D(128, (3, 3), padding='same', kernel_initializer=he_normal, kernel_regularizer=l1_l2_reg))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(Dropout(conv_dropout))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    model.add(Conv2D(256, (3, 3), padding='same', kernel_initializer=he_normal, kernel_regularizer=l1_l2_reg))
    model.add(Conv2D(256, (3, 3), padding='same', kernel_initializer=he_normal, kernel_regularizer=l1_l2_reg))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(Dropout(conv_dropout))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    

    model.add(Flatten())  
    model.add(Dense(512, kernel_initializer=he_normal, kernel_regularizer=l1_l2_reg))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(Dropout(dense_dropout))

    model.add(Dense(512, kernel_initializer=he_normal, kernel_regularizer=l1_l2_reg))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(Dropout(dense_dropout))
    
    model.add(Dense(nmb_classes,activation='sigmoid'))
    
    #lr is handled by learning rate schedule
    opt = keras.optimizers.SGD(lr=0.0)
    
    model.compile(loss=build_masked_loss(K.binary_crossentropy,-1), optimizer=opt)
    return model

def getModel():
    row = metadata_train.loc[0]
    batch_x, batch_y = getPngBatch(folder_path,int(row['FOLDER_NMB']), row['WELL_POS'], row['CHEMBL_ID'], row['SITE'])
    return createModel(batch_x.shape[1:], batch_y.shape[1])


def getPngImage(path, number, a, s):
    folders = ["ERSyto", "ERSytoBleed", "Hoechst", "Mito", "Ph_golgi"]
    J=list()
    global all_max
    for f in folders:
        folder = path + str(number) + "-" + f
        filenames = listdir(folder)
        for filename in filenames:
            if (a.lower() + "_" + s.lower()) in filename.lower():
                imarray = misc.imread(folder+'/'+filename)
                cut_val = np.percentile(imarray, q=99.7)#q=99.99
                imarray = np.clip(imarray, 0, cut_val)
                imarray = np.float32(imarray)
                normalized = (imarray-np.min(imarray))/(np.max(imarray)-np.min(imarray))
                J.append(normalized)
    J = np.stack(J,axis=2)
    shape=J.shape
    I = list()
    I.append(J[:int(shape[0]/2),:int(shape[1]/2),:])
    I.append(J[int(shape[0]/2):,:int(shape[1]/2),:])
    I.append(J[:int(shape[0]/2),int(shape[1]/2):,:])
    I.append(J[int(shape[0]/2):,int(shape[1]/2):,:])
    return I

def getPngBatch(folder_path, folder_number, a, chembl_id, site):

    images = getPngImage(folder_path, folder_number, a, site)

    l=[]
    label = getLabelsForCompound(labels,chembl_id)
    for i in range(0,len(images)):#24):
        l.append(label)
    
    #handle some faulty data
    if np.array(images).shape[3]==10:
        images = np.array(images)[:,:,:,[0,2,4,6,8]]
    
    images = np.asarray(np.float32(images))
    return images, np.asarray(np.float32(l))



def getLabelsForCompound(labels, chembl_cpd):
    l = labels.loc[chembl_cpd].as_matrix()
    return l.astype(int)


def generate_batch(metadata_file, set):
    while 1:
        cnt = 0
        batch_x=[]
        batch_y=[]
        
        for idx,row in metadata_file.iterrows():
            cnt+=1
            folder_number = int(row['FOLDER_NMB'])
            
            broad_id = row['BROAD_ID']
            well_pos = row['WELL_POS']
            chembl_id = row['CHEMBL_ID']
            site = row['SITE']

            b_x, b_y = getPngBatch(folder_path, folder_number, well_pos, chembl_id, site)
            
            batch_x = list(batch_x)
            batch_y = list(batch_y)
            batch_x.extend(b_x)
            batch_y.extend(b_y)
            batch_x = np.asarray(batch_x)
            batch_y = np.asarray(batch_y)
         
            idx = np.arange(len(batch_x))
            batch_x = batch_x[idx]
            batch_y = batch_y[idx]
            
            
            if len(batch_x) >= (4*samples_per_batch):
                yield (batch_x,batch_y)
                batch_x=[]
                batch_y=[]

       
model = getModel()

    
def my_lr_schedule(epoch):
    initial_lrate = 0.01#0.03
    lr = initial_lrate
    for i in range(0,epoch):
        lr = lr * 0.95
    if lr < 0.001:#0.0002:
        lr = 0.001#0.0002
    print("new lr: " + str(lr))
    return lr
    

    
import math

steps_per_epoch = math.floor(metadata_train.shape[0]/samples_per_batch)
val_steps_per_epoch = math.floor(metadata_val.shape[0]/samples_per_batch)#50
test_steps_per_epoch = math.floor(metadata_test.shape[0]/samples_per_batch)

my_lrate = LearningRateScheduler(my_lr_schedule)

save_loss = saveLoss()
roc_auc = ROC_AUC(validation_generator=generate_batch(metadata_val, "val"), validation_steps=val_steps_per_epoch, name='val')
roc_auc_test = ROC_AUC(validation_generator=generate_batch(metadata_test, "test"), validation_steps=test_steps_per_epoch, name='test')
tensorboard_callback = keras.callbacks.TensorBoard(log_dir=log_path, histogram_freq=0, batch_size=24, write_graph=True, write_grads=False, write_images=True, embeddings_freq=0, embeddings_layer_names=None, embeddings_metadata=None)       

save_model_on_epoch_end = saveModelOnEpochEnd()

t_epochs=100

hist = model.fit_generator(callbacks=[save_loss, roc_auc, roc_auc_test, tensorboard_callback, save_model_on_epoch_end,my_lrate], steps_per_epoch=steps_per_epoch, validation_steps=val_steps_per_epoch, epochs=t_epochs, generator=generate_batch(metadata_train, "train"), verbose=1, validation_data=generate_batch(metadata_val, "val"))

del model
K.clear_session()



   