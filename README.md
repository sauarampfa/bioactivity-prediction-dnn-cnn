# Improving image-based compound activity prediction with convolutional neural networks


Bioactivity prediction based on high-throughput images (HTI) from the Cellpainting dataset (https://www.ncbi.nlm.nih.gov/pubmed/27560178). 
A benchmark dataset was created by annotating a subset of the Cellpainting dataset with bioactivity data from the Chembl database (https://www.ebi.ac.uk/chembl/).

A DNN method applied to precalculated image features and a CNN method applied to the cell-images directly.

## CNN
A CNN method was applied to the cell-images to predict bioactivities.

### train.py
Trains the model in batches on the image data.

### predict.py
Load the best model and predicts the bioactivities for each compound in the Cellpainting dataset.

### Predictions.ipynb
Reads and concatenates all the single predictions files.

### data
#### labels.csv 
The extracted labels from the Chembl database. As identifier, the Chembl ID was chosen.

#### meta_*.csv
The train, validation and test split of the data. All information needed to load a specific image are present in these files.

The final model can be downloaded here: https://drive.google.com/file/d/1j7RGp4VK0V3mBiefTWfegwuLtvPmJ42D/view?usp=sharing

## CNN_predictions
### prediction_matrix_auc_morethan_70.csv
Predictions performed by the best performing CNN model. Only assays with AUC>0.7 on the external test set are included.

### prediction_matrix_auc_morethan_90.csv
Predictions performed by the best performing CNN model. Only assays with AUC>0.9 on the external test set are included.

## DNN
A DNN method was applied to precalculated image features to predict bioactivities.

### DNN_descriptors.ipynb
Notebook, loads the descriptor data and trains a DNN.

### data
#### descriptor_*.npy
The image descriptor data split into training, validation and test set.

#### descriptor_labels_*.npy
The labels - accordingly to the descriptor data - split in to training, validation and test set.


